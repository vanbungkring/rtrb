var userModel = require('../models/userModel.js');

/**
 * userController.js
 *
 * @description :: Server-side logic for managing users.
 */
module.exports = {

	/**
	 * userController.list()
	 */
  
	list: function (req, res) {
		userModel.find(function (err, users) {
			if (err) {
				return res.json(500, {
					message: 'Error getting user.'
				});
			}
			return res.json(users);
		});
	},

	/**
	 * userController.show()
	 */
	show: function (req, res) {
		var id = req.params.id;
		userModel.findOne({
			_id: id
		}, function (err, user) {
			if (err) {
				return res.json(500, {
					message: 'Error getting user.'
				});
			}
			if (!user) {
				return res.json(404, {
					message: 'No such user'
				});
			}
			return res.json(user);
		});
	},

	/**
	 * userController.create()
	 */
	create: function (req, res) {
		var user = new userModel({
			userName: req.body.userName,
			email: req.body.email,
			userRole: req.body.userRole,
			token: req.body.token,
			createdAt: req.body.createdAt,
			updatedAt: req.body.updatedAt,
			password: req.body.password,
			firstName: req.body.firstName,
			lastName: req.body.lastName,
			token: req.body.token
		});

		user.save(function (err, user) {
			if (err) {
				return res.json(500, {
					message: 'Error saving user',
					error: err
				});
			}
			return res.json({
				message: 'saved',
				_id: user._id
			});
		});
	},

	/**
	 * userController.update()
	 */
	update: function (req, res) {
		var id = req.params.id;
		userModel.findOne({
			_id: id
		}, function (err, user) {
			if (err) {
				return res.json(500, {
					message: 'Error saving user',
					error: err
				});
			}
			if (!user) {
				return res.json(404, {
					message: 'No such user'
				});
			}

			user.userName = req.body.userName ? req.body.userName : user.userName;
			user.email = req.body.email ? req.body.email : user.email;
			user.userRole = req.body.userRole ? req.body.userRole : user.userRole;
			user.token = req.body.token ? req.body.token : user.token;
			user.createdAt = req.body.createdAt ? req.body.createdAt : user.createdAt;
			user.updatedAt = req.body.updatedAt ? req.body.updatedAt : user.updatedAt;
			user.password = req.body.password ? req.body.password : user.password;
			user.firstName = req.body.firstName ? req.body.firstName : user.firstName;
			user.lastName = req.body.lastName ? req.body.lastName : user.lastName;
			user.token = req.body.token ? req.body.token : user.token;

			user.save(function (err, user) {
				if (err) {
					return res.json(500, {
						message: 'Error getting user.'
					});
				}
				if (!user) {
					return res.json(404, {
						message: 'No such user'
					});
				}
				return res.json(user);
			});
		});
	},

	/**
	 * userController.remove()
	 */
	remove: function (req, res) {
		var id = req.params.id;
		userModel.findByIdAndRemove(id, function (err, user) {
			if (err) {
				return res.json(500, {
					message: 'Error getting user.'
				});
			}
			return res.json(user);
		});
	}
};
