var taxObjectModel = require('../models/taxObjectModel.js');

/**
 * taxObjectController.js
 *
 * @description :: Server-side logic for managing taxObjects.
 */
module.exports = {

	/**
	 * taxObjectController.list()
	 */
	list: function (req, res) {
		taxObjectModel.find(function (err, taxObjects) {
			if (err) {
				return res.json(500, {
					message: 'Error getting taxObject.'
				});
			}
			return res.json(taxObjects);
		});
	},

	/**
	 * taxObjectController.show()
	 */
	show: function (req, res) {
		var id = req.params.id;
		taxObjectModel.findOne({
			_id: id
		}, function (err, taxObject) {
			if (err) {
				return res.json(500, {
					message: 'Error getting taxObject.'
				});
			}
			if (!taxObject) {
				return res.json(404, {
					message: 'No such taxObject'
				});
			}
			return res.json(taxObject);
		});
	},

	/**
	 * taxObjectController.create()
	 */
	create: function (req, res) {
		var taxObject = new taxObjectModel({
			name: req.body.name,
			type: req.body.type,
			address: req.body.address,
			kecamatan: req.body.kecamatan,
			kelurahan: req.body.kelurahan,
			geoLocation.latitude: req.body.latitude
			geoLocation.longitude: req.body.longitude
      geoLocation.creator: req.body.userId
		});

		taxObject.save(function (err, taxObject) {
			if (err) {
				return res.json(500, {
					message: 'Error saving taxObject',
					error: err
				});
			}
			return res.json({
				message: 'saved',
				_id: taxObject._id
			});
		});
	},

	/**
	 * taxObjectController.update()
	 */
	update: function (req, res) {
		var id = req.params.id;
		taxObjectModel.findOne({
			_id: id
		}, function (err, taxObject) {
			if (err) {
				return res.json(500, {
					message: 'Error saving taxObject',
					error: err
				});
			}
			if (!taxObject) {
				return res.json(404, {
					message: 'No such taxObject'
				});
			}

			taxObject.name = req.body.name ? req.body.name : taxObject.name;
			taxObject.type = req.body.type ? req.body.type : taxObject.type;
			taxObject.address = req.body.address ? req.body.address : taxObject.address;
			taxObject.kecamatan = req.body.kecamatan ? req.body.kecamatan : taxObject.kecamatan;
			taxObject.kelurahan = req.body.kelurahan ? req.body.kelurahan : taxObject.kelurahan;

			taxObject.save(function (err, taxObject) {
				if (err) {
					return res.json(500, {
						message: 'Error getting taxObject.'
					});
				}
				if (!taxObject) {
					return res.json(404, {
						message: 'No such taxObject'
					});
				}
				return res.json(taxObject);
			});
		});
	},

	/**
	 * taxObjectController.remove()
	 */
	remove: function (req, res) {
		var id = req.params.id;
		taxObjectModel.findByIdAndRemove(id, function (err, taxObject) {
			if (err) {
				return res.json(500, {
					message: 'Error getting taxObject.'
				});
			}
			return res.json(taxObject);
		});
	}
};
