var bussinessDomainModel = require('../models/bussinessDomainModel.js');

/**
 * bussinessDomainController.js
 *
 * @description :: Server-side logic for managing bussinessDomains.
 */
module.exports = {

    /**
     * bussinessDomainController.list()
     */
    list: function(req, res) {
        bussinessDomainModel.find(function(err, bussinessDomains){
            if(err) {
                return res.json(500, {
                    message: 'Error getting bussinessDomain.'
                });
            }
            return res.json(bussinessDomains);
        });
    },

    /**
     * bussinessDomainController.show()
     */
    show: function(req, res) {
        var id = req.params.id;
        bussinessDomainModel.findOne({_id: id}, function(err, bussinessDomain){
            if(err) {
                return res.json(500, {
                    message: 'Error getting bussinessDomain.'
                });
            }
            if(!bussinessDomain) {
                return res.json(404, {
                    message: 'No such bussinessDomain'
                });
            }
            return res.json(bussinessDomain);
        });
    },

    /**
     * bussinessDomainController.create()
     */
    create: function(req, res) {
        var bussinessDomain = new bussinessDomainModel({			name : req.body.name,			remarks : req.body.remarks,			createdAt : req.body.createdAt,			updatedAt : req.body.updatedAt
        });

        bussinessDomain.save(function(err, bussinessDomain){
            if(err) {
                return res.json(500, {
                    message: 'Error saving bussinessDomain',
                    error: err
                });
            }
            return res.json({
                message: 'saved',
                _id: bussinessDomain._id
            });
        });
    },

    /**
     * bussinessDomainController.update()
     */
    update: function(req, res) {
        var id = req.params.id;
        bussinessDomainModel.findOne({_id: id}, function(err, bussinessDomain){
            if(err) {
                return res.json(500, {
                    message: 'Error saving bussinessDomain',
                    error: err
                });
            }
            if(!bussinessDomain) {
                return res.json(404, {
                    message: 'No such bussinessDomain'
                });
            }

            bussinessDomain.name =  req.body.name ? req.body.name : bussinessDomain.name;			bussinessDomain.remarks =  req.body.remarks ? req.body.remarks : bussinessDomain.remarks;			bussinessDomain.createdAt =  req.body.createdAt ? req.body.createdAt : bussinessDomain.createdAt;			bussinessDomain.updatedAt =  req.body.updatedAt ? req.body.updatedAt : bussinessDomain.updatedAt;			
            bussinessDomain.save(function(err, bussinessDomain){
                if(err) {
                    return res.json(500, {
                        message: 'Error getting bussinessDomain.'
                    });
                }
                if(!bussinessDomain) {
                    return res.json(404, {
                        message: 'No such bussinessDomain'
                    });
                }
                return res.json(bussinessDomain);
            });
        });
    },

    /**
     * bussinessDomainController.remove()
     */
    remove: function(req, res) {
        var id = req.params.id;
        bussinessDomainModel.findByIdAndRemove(id, function(err, bussinessDomain){
            if(err) {
                return res.json(500, {
                    message: 'Error getting bussinessDomain.'
                });
            }
            return res.json(bussinessDomain);
        });
    }
};