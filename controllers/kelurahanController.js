var kelurahanModel = require('../models/kelurahanModel.js');

/**
 * kelurahanController.js
 *
 * @description :: Server-side logic for managing kelurahans.
 */
module.exports = {

    /**
     * kelurahanController.list()
     */
    list: function(req, res) {
        kelurahanModel.find(function(err, kelurahans){
            if(err) {
                return res.json(500, {
                    message: 'Error getting kelurahan.'
                });
            }
            return res.json(kelurahans);
        });
    },

    /**
     * kelurahanController.show()
     */
    show: function(req, res) {
        var id = req.params.id;
        kelurahanModel.findOne({_id: id}, function(err, kelurahan){
            if(err) {
                return res.json(500, {
                    message: 'Error getting kelurahan.'
                });
            }
            if(!kelurahan) {
                return res.json(404, {
                    message: 'No such kelurahan'
                });
            }
            return res.json(kelurahan);
        });
    },

    /**
     * kelurahanController.create()
     */
    create: function(req, res) {
        var kelurahan = new kelurahanModel({			name : req.body.name,			remarks : req.body.remarks,			parentId : req.body.parentId,			createdAt : req.body.createdAt,			updatedAt : req.body.updatedAt
        });

        kelurahan.save(function(err, kelurahan){
            if(err) {
                return res.json(500, {
                    message: 'Error saving kelurahan',
                    error: err
                });
            }
            return res.json({
                message: 'saved',
                _id: kelurahan._id
            });
        });
    },

    /**
     * kelurahanController.update()
     */
    update: function(req, res) {
        var id = req.params.id;
        kelurahanModel.findOne({_id: id}, function(err, kelurahan){
            if(err) {
                return res.json(500, {
                    message: 'Error saving kelurahan',
                    error: err
                });
            }
            if(!kelurahan) {
                return res.json(404, {
                    message: 'No such kelurahan'
                });
            }

            kelurahan.name =  req.body.name ? req.body.name : kelurahan.name;			kelurahan.remarks =  req.body.remarks ? req.body.remarks : kelurahan.remarks;			kelurahan.parentId =  req.body.parentId ? req.body.parentId : kelurahan.parentId;			kelurahan.createdAt =  req.body.createdAt ? req.body.createdAt : kelurahan.createdAt;			kelurahan.updatedAt =  req.body.updatedAt ? req.body.updatedAt : kelurahan.updatedAt;			
            kelurahan.save(function(err, kelurahan){
                if(err) {
                    return res.json(500, {
                        message: 'Error getting kelurahan.'
                    });
                }
                if(!kelurahan) {
                    return res.json(404, {
                        message: 'No such kelurahan'
                    });
                }
                return res.json(kelurahan);
            });
        });
    },

    /**
     * kelurahanController.remove()
     */
    remove: function(req, res) {
        var id = req.params.id;
        kelurahanModel.findByIdAndRemove(id, function(err, kelurahan){
            if(err) {
                return res.json(500, {
                    message: 'Error getting kelurahan.'
                });
            }
            return res.json(kelurahan);
        });
    }
};