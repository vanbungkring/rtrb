var kecamatanModel = require('../models/kecamatanModel.js');

/**
 * kecamatanController.js
 *
 * @description :: Server-side logic for managing kecamatans.
 */
module.exports = {

	/**
	 * kecamatanController.list()
	 */
	list: function (req, res) {
		kecamatanModel.find(function (err, kecamatans) {
			if (err) {
				return res.json(500, {
					message: 'Error getting kecamatan.'
				});
			}
			return res.json(kecamatans);
		});
	},

	/**
	 * kecamatanController.show()
	 */
	show: function (req, res) {
		var id = req.params.id;
		kecamatanModel.findOne({
			_id: id
		}, function (err, kecamatan) {
			if (err) {
				return res.json(500, {
					message: 'Error getting kecamatan.'
				});
			}
			if (!kecamatan) {
				return res.json(404, {
					message: 'No such kecamatan'
				});
			}
			return res.json(kecamatan);
		});
	},

	/**
	 * kecamatanController.create()
	 */
	create: function (req, res) {
		var kecamatan = new kecamatanModel({
			name: req.body.name,
			remarks: req.body.remarks,
			createdAt: req.body.createdAt,
			updatedAt: req.body.updatedAt
		});

		kecamatan.save(function (err, kecamatan) {
			if (err) {
				return res.json(500, {
					message: 'Error saving kecamatan',
					error: err
				});
			}
			return res.json({
				message: 'saved',
				_id: kecamatan._id
			});
		});
	},

	/**
	 * kecamatanController.update()
	 */
	update: function (req, res) {
		var id = req.params.id;
		kecamatanModel.findOne({
			_id: id
		}, function (err, kecamatan) {
			if (err) {
				return res.json(500, {
					message: 'Error saving kecamatan',
					error: err
				});
			}
			if (!kecamatan) {
				return res.json(404, {
					message: 'No such kecamatan'
				});
			}

			kecamatan.name = req.body.name ? req.body.name : kecamatan.name;
			kecamatan.remarks = req.body.remarks ? req.body.remarks : kecamatan.remarks;
			kecamatan.createdAt = req.body.createdAt ? req.body.createdAt : kecamatan.createdAt;
			kecamatan.updatedAt = req.body.updatedAt ? req.body.updatedAt : kecamatan.updatedAt;

			kecamatan.save(function (err, kecamatan) {
				if (err) {
					return res.json(500, {
						message: 'Error getting kecamatan.'
					});
				}
				if (!kecamatan) {
					return res.json(404, {
						message: 'No such kecamatan'
					});
				}
				return res.json(kecamatan);
			});
		});
	},

	/**
	 * kecamatanController.remove()
	 */
	remove: function (req, res) {
		var id = req.params.id;
		kecamatanModel.findByIdAndRemove(id, function (err, kecamatan) {
			if (err) {
				return res.json(500, {
					message: 'Error getting kecamatan.'
				});
			}
			return res.json(kecamatan);
		});
	}
};