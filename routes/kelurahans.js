var express = require('express');
var router = express.Router();
var kelurahanController = require('../controllers/kelurahanController.js');

/*
 * GET
 */
router.get('/', function(req, res) {
    kelurahanController.list(req, res);
});

/*
 * GET
 */
router.get('/:id', function(req, res) {
    kelurahanController.show(req, res);
});

/*
 * POST
 */
router.post('/', function(req, res) {
    kelurahanController.create(req, res);
});

/*
 * PUT
 */
router.put('/:id', function(req, res) {
    kelurahanController.update(req, res);
});

/*
 * DELETE
 */
router.delete('/:id', function(req, res) {
    kelurahanController.remove(req, res);
});

module.exports = router;