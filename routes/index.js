var auth = require ('../controllers/authController');
var auth = require ('../controllers/authController');
module.exports = function (app, passport) {
  app.get('/',auth.index);
  app.get('/dashboard',auth.dashboard);
  app.post('/', passport.authenticate('local-login', {
		successRedirect: '/dashboard', // redirect to the secure profile section
		failureRedirect: '/dashboard', // redirect back to the signup page if there is an error
		failureFlash: true // allow flash messages
	}));
	app.get('/logout', function (req, res) {
		req.logout();
		req.session.destroy();
		res.redirect('/');
	});
	app.get('*', function(err,req, res,next){
	});
}

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next, app) {
	if (req.isAuthenticated()) {
		if (req.user.userRole == 2) {
			userModel.findOne({
				"_id": req.user.masjidId
			}, function (err, result) {
				res.local.masjid = result;
			})
		}
		return next();
	} else {
		res.redirect('/');
	}
}
