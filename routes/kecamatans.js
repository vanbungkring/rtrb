var express = require('express');
var router = express.Router();
var kecamatanController = require('../controllers/kecamatanController.js');

/*
 * GET
 */
router.get('/', function(req, res) {
    kecamatanController.list(req, res);
});

/*
 * GET
 */
router.get('/:id', function(req, res) {
    kecamatanController.show(req, res);
});

/*
 * POST
 */
router.post('/', function(req, res) {
    kecamatanController.create(req, res);
});

/*
 * PUT
 */
router.put('/:id', function(req, res) {
    kecamatanController.update(req, res);
});

/*
 * DELETE
 */
router.delete('/:id', function(req, res) {
    kecamatanController.remove(req, res);
});

module.exports = router;