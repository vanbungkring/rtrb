var express = require('express');
var router = express.Router();
var bussinessDomainController = require('../controllers/bussinessDomainController.js');

/*
 * GET
 */
router.get('/', function(req, res) {
    bussinessDomainController.list(req, res);
});

/*
 * GET
 */
router.get('/:id', function(req, res) {
    bussinessDomainController.show(req, res);
});

/*
 * POST
 */
router.post('/', function(req, res) {
    bussinessDomainController.create(req, res);
});

/*
 * PUT
 */
router.put('/:id', function(req, res) {
    bussinessDomainController.update(req, res);
});

/*
 * DELETE
 */
router.delete('/:id', function(req, res) {
    bussinessDomainController.remove(req, res);
});

module.exports = router;