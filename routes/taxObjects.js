var express = require('express');
var router = express.Router();
var taxObjectController = require('../controllers/taxObjectController.js');

/*
 * GET
 */
router.get('/', function(req, res) {
    taxObjectController.list(req, res);
});

/*
 * GET
 */
router.get('/:id', function(req, res) {
    taxObjectController.show(req, res);
});

/*
 * POST
 */
router.post('/', function(req, res) {
    taxObjectController.create(req, res);
});

/*
 * PUT
 */
router.put('/:id', function(req, res) {
    taxObjectController.update(req, res);
});

/*
 * DELETE
 */
router.delete('/:id', function(req, res) {
    taxObjectController.remove(req, res);
});

module.exports = router;