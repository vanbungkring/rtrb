var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var passport = require('passport');
var mongoose = require('mongoose');
var config = require('config');
var flash = require('connect-flash');
var session = require('express-session');
var MongoStore = require('connect-mongo')({
	session: session
});
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var engine = require('ejs-locals');
var app = express();

// view engine setup
console.log(config);
var configuration = config.get('rtrb');
app.set('global', configuration);
mongoose.connect(configuration.dbConfig.host);
app.set('views', path.join(__dirname, 'views'));
app.engine('ejs', engine);
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
var sessionOpts = {
	resave: true,
	saveUninitialized: true,
  store: new MongoStore({
     url: configuration.dbConfig.host,
     collection : 'rtrb.session'
   }),
	saveUninitialized: false,
	secret: configuration.cookie.secret,
}
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(cookieParser());
app.use(cookieParser(configuration.cookie.secret))
app.use(session(sessionOpts))
app.use(flash()); // use connect-flash for flash messages stored in session
require('./helper/passport')(passport); // pass passport for configuration
app.use(passport.initialize())
app.use(passport.session({
	maxAge: new Date(Date.now() + 3600000)
}))
require('./routes/index.js')(app, passport);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	app.use(function (err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	});
});


module.exports = app;
