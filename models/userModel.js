var mongoose = require('mongoose');
var Schema   = mongoose.Schema,
bcrypt = require('bcrypt'),
SALT_WORK_FACTOR = 10;

var userSchema = new Schema({	"userName" : String,	"email" : String,	"userRole" : Number,	"token" : String,	"createdAt" : Date,	"updatedAt" : String,	"password" : String,	"firstName" : String,	"lastName" : String,	"token" : String});
///presave
// on every save, add the date
userSchema.pre('save', function (next) {
	var user = this;
	// get the current date
	var currentDate = new Date();

	// change the updated_at field to current date
	user.updated_at = currentDate;

	// if created_at doesn't exist, add to that field
	if (!user.created_at)
		user.created_at = currentDate;

	// only hash the password if it has been modified (or is new)
	if (!user.isModified('password')) return next();

	// generate a salt
	bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
		if (err) return next(err);

		// hash the password using our new salt
		bcrypt.hash(user.password, salt, function (err, hash) {
			if (err) return next(err);

			// override the cleartext password with the hashed one
			user.password = hash;
			next();
		});
	});
});

userSchema.methods.generateHash = function (password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(SALT_WORK_FACTOR), null);
};

// checking if password is valid
userSchema.methods.validPassword = function (password) {
	return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('retrib.User', userSchema);
