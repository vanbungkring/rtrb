var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var kecamatanSchema = new Schema({	"name" : String,	"remarks" : String,	"createdAt" : Date,	"updatedAt" : Date});

module.exports = mongoose.model('retrib.kecamatan', kecamatanSchema);
