var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var kelurahanSchema = new Schema({	"name" : String,	"remarks" : String,	"parentId" : {	 	type: Schema.Types.ObjectId,	 	ref: 'retrib.kecamatan'	},	"createdAt" : Date,	"updatedAt" : Date});

module.exports = mongoose.model('retrib.kelurahan', kelurahanSchema);
