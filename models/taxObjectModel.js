var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var taxObjectSchema = new Schema({
	"name": String,
	"type": {
		type: Schema.Types.ObjectId,
		ref: 'retrib.bussinessDomain'
	},
	"address": String,
	"kecamatan": {
		type: Schema.Types.ObjectId,
		ref: 'retrib.kecamatan'
	},
	"creator" : {
	 	type: Schema.Types.ObjectId,
	 	ref: 'retrib.User'
	},
	"kelurahan": {
		type: Schema.Types.ObjectId,
		ref: 'retrib.kelurahan'
	},
	"geoLocation": {
		latitude: Number,
		longitude: Number,
	}
});

module.exports = mongoose.model('taxObject', taxObjectSchema);
